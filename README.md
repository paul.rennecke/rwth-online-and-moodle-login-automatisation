# RWTH online and moodle Login-Automatisation

RWTHonline and moodle Login-Automatisation Simple automatic redirect to Login-Page of RWTH online or moodle.

And automatic filling of the login data with an integrated encrypted password memory.

Simple one click install via Tampermonkey browser extension https://www.tampermonkey.net/